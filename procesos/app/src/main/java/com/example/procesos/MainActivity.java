package com.example.procesos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.AsyncTaskLoader;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import java.nio.channels.AsynchronousChannelGroup;

public class MainActivity extends AppCompatActivity {

    private Button btnValidar;
    private EditText etUsuario, etClave;
    private ProgressBar progressbar1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnValidar = (Button)findViewById(R.id.btnValidar);
        etUsuario = (EditText)findViewById(R.id.etUsuario);
        etClave = (EditText)findViewById(R.id.etClave);
        progressbar1 = (ProgressBar)findViewById(R.id.progressBar1);

        btnValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            new Task1().execute(etUsuario.getText().toString());
            }
        });
    }

    class Task1 extends AsyncTask<String, Void, String>{

        @Override
        protected void onPreExecute() {
            progressbar1.setVisibility(View.VISIBLE);
            btnValidar.setEnabled(false);

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                Thread.sleep(5000);
            }catch (InterruptedException e) {
                e.printStackTrace();
        }
            return strings[0];
    }
        @Override
        protected void onPostExecute(String s) {
        progressbar1.setVisibility(View.INVISIBLE);
        btnValidar.setEnabled(true);
            Intent intent = new Intent(MainActivity.this, ResultadoActivity.class);
            intent.putExtra("usuario",etUsuario.getText().toString());
            startActivity(intent);
        }

    }

}